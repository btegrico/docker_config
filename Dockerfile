FROM odoo:11.0

# Elevate to root
USER root

# Provide & install all given Python requirements
RUN set -x; \
    apt-get update \
    && apt-get install -y python3-pip python3-dev \
    && cd /usr/local/bin \
    && ln -s /usr/bin/python3 python \
    && pip3 install --upgrade pip

RUN pip3 install psycogreen mammoth pysftp pdfconv cachetools

# Switch back to `odoo' user
USER odoo
