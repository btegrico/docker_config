# Odoo project

## For successfully install  Odoo you should:

1. build image from Dockerfile in this folder

    sudo docker build -t jessie/odoo11 .
    
2. start containers

    docker-compose up
